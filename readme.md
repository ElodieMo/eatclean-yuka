React - Application de scan de nourriture

Contexte du projet

Vous connaissez peut-être Yuka : cette application permet de scanner des code-barres de produits divers afin de connaître leurs compositions.

​

Avant de démarrer ce brief, installez l'application sur votre smartphone et voyez ce qu'il est possible de faire avec.

​

L'application se base sur l'API OpenFoodFact pour afficher ses données : https://world-fr.openfoodfacts.org/data

​

Lors du scan du code-barre d'un produit, un code est lu (par exemple 737628064502). Il suffit ensuite d'appeler le fichier JSON correspondant au code du prouit pour avoir les informations correspondantes, ex: https://world.openfoodfacts.org/api/v0/product/737628064502.json

​

Tu peux aussi voir sur le site de OpenFoodFact diverses informations qui ont été récupérées de l'API : https://world-fr.openfoodfacts.org/produit/0737628064502

​

Il est aussi possible de chercher des produits par leur nom. Vous trouverez la méthode ici : https://wiki.openfoodfacts.org/API/Read/Search (pensez que vous voulez récupérer un JSON).

​

Prenez le temps d'étudier les données renvoyées par le JSON avant de continuer la lecture de brief.

Polices :

​

    Titres : https://fonts.google.com/specimen/Lexend+Deca (SemiBold 600)
    Les autres textes : https://fonts.google.com/specimen/Questrial (Regular 400)

​

Couleurs :

​

    Accent color : #6f9939
    Text primary color : #000000
    Text secondary color: #c9ccd2

​
Organisation du travail :

En groupe de 4 sur deux semaines :

​

le matin, travail en autonomie pour acquérir les notions nécessaires au projet

l'après-midi, travail en groupe

Ce que vous devez faire :

​

    créez un wireframe sur papier du site, en respectant la demande du client. Vous pouvez utiliser le crazy8 dans un premier temps. Faites valider le wireframe par votre formateur⋅trice
    créez la maquette en vous basant sur le design fourni et sur le wireframe validé et faites la valider par votre formateur⋅trice
    créez une product backlog avec les tâches à réaliser et faite-le valider par votre formateur⋅trice
    assignez les tâches en utilisant un kanban
    utilisez React pour créer les fonctionnalités
    versionnez votre projet sur Gitlab en utilisant le workflow Gitflow

Modalités pédagogiques

En groupe de 4 sur deux semaines
Livrables

Le wireframe, la maquette, le backlog product, le kanban et le lien vers le Gitlab du projet

Une démo du prototype est attendue à la fin de chaque sprint, à partir de la branche main.
Chaque fonctionnalité doit passer par une Pull Request (/merge request) et être validé par un membre de l'équipe, puis par votre formateur⋅trice, avant d'être réintégrée à la branche develop.